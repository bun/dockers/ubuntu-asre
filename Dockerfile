# Extend the ubuntu docker with some apps

FROM gns3/ubuntu:focal

RUN set -x \
    && apt -yqq update \
    && apt -y install --no-install-recommends isc-dhcp-client dnsutils\
    && apt -y install snmp snmpd libsnmp-dev\
    && apt -y install python3 build-essential libssl-dev libffi-dev \
    && apt -y install python3-pip python3-dev openssh-server netcat \
    && apt -y install nmap wget \
    && pip install cryptography \
    && pip install paramiko \
    && rm -rf /var/lib/apt/lists/* 
CMD ["bash"]