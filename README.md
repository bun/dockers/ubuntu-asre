# MkDocs Builder for HEIA-FR
The CI/CD of this repo builds and publish a Docker image based on Ubuntu with some additional packages.

## Description

This docker image is based on [gns3/ubuntu](https://hub.docker.com/r/gns3/ubuntu) docker image and add following packages:

- snmpd
- python3
- pip
- paramiko (python Library for SSH)
- sshd
- netcat
- nmap

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t ubuntu-asre .
```


## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository--> Tag" menu to add the latest version number.

## How to use this image

To download and use this image, just type:
```
docker pull registry.forge.hefr.ch/bun/dockers/ubuntu-asre
```

Then you can use it, by accessing it's shell:
```
docker run --rm -it registry.forge.hefr.ch/francois.buntschu/ubuntu-asre
```
## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/


<hr>
(c) F. Buntschu 15.02.2022